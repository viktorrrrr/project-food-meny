var input=document.querySelector(".inputText");
var items=document.querySelector(".items");
var modalDiv=document.querySelector(".modal-body");
var mealDiv=document.querySelector(".headMeal")


const jela=[
{
meal:"pasulj",
mealItems:["pasulj","voda","so","slanina"],
grams:"50",
serviranje:["ovde","za poneti"],
},
{
meal:"cevapi",
mealItems:["so","mlevenomeso","luk","ulje"],
grams:"70",
serviranje:["ovde","za poneti"],  
}
]
localStorage.setItem('key', JSON.stringify(jela));
const array=[];


function getInput(){
    if(input.value.length > 1){
        return input.value;
    }
}

function checkArrays(m){
    let noMeals=[];
    var matches=0;
    for(let i=0;i<array.length;i++){
        for(let j=0;j<jela[m]["mealItems"].length;j++){
            if(array[i]===jela[m]["mealItems"][j]){
                matches++;
                if(matches==4){
                    createHeadMeal(jela[m]["meal"])
                    break;
                }
            }
        }
    }
    if(matches>=2 & matches<=3){
        
            for(let j=0;j<jela[m]["mealItems"].length;j++){
                for(let i=0;i<array.length;i++){
                    if(!array.includes(jela[m]["mealItems"][j]) && !noMeals.includes(jela[m]["mealItems"][j])){
                        noMeals.push(jela[m]["mealItems"][j]);
                    }
                }
            }
          
        createMeal(jela[m]["meal"],noMeals)
              
    }
}  

function checkItems(){

   for(let i=0;i<jela.length;i++){
        checkArrays(i);
    }
 }

function createDiv(a){
    input.value="";
   
    if(!array.includes(a)){
        array.push(a);
        items.innerHTML+=`<div class="card" id="kartica" style="width: 15rem;">
        <img class="card-img-top imgsize" src="${a}.png" alt="Vasa namernica">
        <div class="card-body">
        <h5 class="card-title">${a}</h5>
        <p class="card-text">Vasa namernica</p>
        <button class="btn btn-primary dugme" onclick="removeDiv(this);deleteArray();" >Obrisi</button>
        </div>
        </div>`
    }
    console.log(array);
    
}

function removeDiv(e){
    e.parentElement.parentElement.remove()
}
    
function deleteArray(){
    array.pop();
//     const index = array.indexOf(5);
// if (index > -1) {
//   array.splice(index, 1);
// }
}
 
 function createModal(){
     let duplicateModal=[];
    for(let j=0;j<jela.length;j++){
        for(let i=0;i<jela[j]["mealItems"].length;i++){
            if(!duplicateModal.includes(jela[j]["mealItems"][i])){
            duplicateModal.push(jela[j]["mealItems"][i]);
            modalDiv.innerHTML+=`<div class="card" style="width: 12rem;">
            <img class="card-img-top imgsize" src="${jela[j]["mealItems"][i]}.png" alt="Vasa namernica">
            <div class="card-body">
            <h5 class="card-title">${jela[j]["mealItems"][i]}</h5>
            <p class="card-text">Vasa namernica</p>
            <button class="btn btn-dark dugme" onclick="createDiv('${jela[j]["mealItems"][i]}')" >+</button>
            </div>
            </div>`}
        }
    }

    
}

function deleteModal(){
    modalDiv.innerHTML="";
}

function showItems(){
  
    switch(getInput()){
     case "so":
         createDiv("so");
         break;
     case "pasulj":
            createDiv("pasulj");
            break;
        case "voda":
            createDiv("voda");
            break;
        case "slanina":
            createDiv("slanina");
            break;
        case "mleveno meso":
            createDiv("mlevenomeso");
            break;
        case "luk":
            createDiv("luk");
            break;
        case "ulje":
            createDiv("ulje");
            break;
        default:
            createDiv(getInput());
            
    }
    
}

function createHeadMeal(meal){
    mealDiv.innerHTML +=`<div class="card positionMeal" style="width: 30rem;">
    <img class="card-img-top" src="${meal}1.jpg" alt="Card image cap">
    <div class="card-body">
    <h5 class="card-title">${meal}</h5>
    <p class="card-text">Ovo je jelo koje mozete spremiti</p>
    <a href="https://www.recepti.com/kuvar/glavna-jela/201-pasulj-prebranac" class="btn btn-primary">Saznaj vise</a>
    </div>
    </div>`
}
function createMeal(meal,noMeals){
    mealDiv.innerHTML +=`<div class="card positionMeal" style="width: 30rem;">
    <img class="card-img-top" src="${meal}1.jpg" alt="Card image cap">
    <div class="card-body">
    <h5 class="card-title">${meal}</h5>
    <p class="card-text">Za spremanje ovog jela fali vam ${noMeals}</p>
    <a href="https://www.recepti.com/kuvar/glavna-jela/201-pasulj-prebranac" class="btn btn-primary">Saznaj vise</a>
    </div>
    </div>`
}

function clearHeadMeal(){
    mealDiv.innerHTML ="";
}

var retrievedObject = localStorage.getItem('helpObj');
function retrieveObject(){
    const niz=[]
    retrievedObject=JSON.parse(retrievedObject);
    niz.push(retrievedObject["2"]);
    var dataImage = localStorage.getItem('imgData');
    bannerImg = document.getElementById('tableBanner');
    bannerImg.src = "data:image/png;base64," + dataImage;
    document.querySelector(".secondObject").innerHTML=`<p>Za spremanje ${retrievedObject[0]} jela, potrebni sastojci su ${retrievedObject[2]}<p> `;
    console.log(niz)
   
  }