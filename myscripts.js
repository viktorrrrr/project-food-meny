var input=document.querySelector(".inputText");
var items=document.querySelector(".items");
var modalDiv=document.querySelector(".modal-body");
var mealDiv=document.querySelector(".headMeal")

const jela=[   ["pasulj","pasulj","voda","so","slanina"],["cevapi","so","mlevenomeso","luk","ulje"]   ];
const array=[];

function getInput(){
    if(input.value.length > 1){
        return input.value;
    }
}

function checkItems(){
    var matches=0;
    let m=0;
    for(let i=0;i<array.length;i++){
        for(let j=1;j<jela[m].length;j++){
            if(array[i]===jela[m][j]){
                matches++;
                if(matches==4){
                    createHeadMeal(jela[m][0])
                    break;
                }
            }
        }
        
    }

    matches=0;

    for(let i=0;i<array.length;i++){

        for(let j=1;j<jela[1].length;j++){
        
            if(array[i]===jela[1][j]){
                matches++;
                if(matches==4){
                    return createHeadMeal(jela[1][0])
                    
                }
                
            }
        }
        
    }

}

function createDiv(a){
    input.value="";
    array.push(a);
    console.log(array)
     items.innerHTML+=`<div class="card" id="kartica" style="width: 15rem;">
    <img class="card-img-top imgsize" src="${a}.png" alt="Vasa namernica">
    <div class="card-body">
    <h5 class="card-title">${a}</h5>
    <p class="card-text">Vasa namernica</p>
    <button class="btn btn-primary dugme" onclick="removeDiv(this);deleteArray();" >Obrisi</button>
    </div>
    </div>`
}

function removeDiv(e){
    e.parentElement.parentElement.remove()
}
    
function deleteArray(){
    array.pop();
}
 
 function createModal(){
    for(let j=0;j<jela.length;j++){
        for(let i=1;i<jela[j].length;i++){
            modalDiv.innerHTML+=`<div class="card" style="width: 12rem;">
            <img class="card-img-top imgsize" src="${jela[j][i]}.png" alt="Vasa namernica">
            <div class="card-body">
            <h5 class="card-title">${jela[j][i]}</h5>
            <p class="card-text">Vasa namernica</p>
            <button class="btn btn-dark dugme" onclick="createDiv('${jela[j][i]}')" >+</button>
            </div>
            </div>`
        }
    }

    
}

function deleteModal(){
    modalDiv.innerHTML="";
}

function showItems(){
  
    switch(getInput()){
     case "so":
         createDiv("so");
         break;
     case "pasulj":
            createDiv("pasulj");
            break;
        case "voda":
            createDiv("voda");
            break;
        case "slanina":
            createDiv("slanina");
            break;
        case "mleveno meso":
            createDiv("mlevenomeso");
            break;
        case "luk":
            createDiv("luk");
            break;
        case "ulje":
            createDiv("ulje");
            break;
        default:
            alert("Ne postoji ta namernica")
            
    }
    
}

function createHeadMeal(meal){
    mealDiv.innerHTML +=`<div class="card positionMeal" style="width: 30rem;">
    <img class="card-img-top" src="${meal}1.jpg" alt="Card image cap">
    <div class="card-body">
    <h5 class="card-title">${meal}</h5>
    <p class="card-text">Ovo je jelo koje mozete spremiti</p>
    <a href="https://www.recepti.com/kuvar/glavna-jela/201-pasulj-prebranac" class="btn btn-primary">Saznaj vise</a>
    </div>
    </div>`
}

function clearHeadMeal(){
    mealDiv.innerHTML ="";
}